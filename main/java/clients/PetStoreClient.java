package clients;

import core.ApiCaller;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import model.SimplePetResponse;
import model.pet.Pet;
import model.pet.Status;
import okhttp3.MultipartBody;
import retrofit2.Response;

import java.util.List;

@AllArgsConstructor
public class PetStoreClient {

    private final PetStoreService apis;

    public PetStoreClient(ApiCaller apiCaller) {
        String URL = "https://petstore.swagger.io";
        apis = apiCaller.getServiceApiCalls(URL, PetStoreService.class);
    }

    @SneakyThrows
    public Response<Pet> addPet(Pet pet) {
        return apis.addPet(pet).execute();
    }

    @SneakyThrows
    public Response<Pet> updatePet(Pet pet) {
        return apis.updatePet(pet).execute();
    }

    @SneakyThrows
    public Response<List<Pet>> getPetsWithStatus(Status status) {
        return apis.getPetsWithStatus(status).execute();
    }

    @SneakyThrows
    public Response<Pet> getPet(Integer petId) {
        return apis.getPetWithId(petId).execute();
    }

    @SneakyThrows
    public Response<SimplePetResponse> updatePet(Integer petId, String newName, Status newStatus) {
        return apis.updatePet(petId, newName, newStatus).execute();
    }

    @SneakyThrows
    public Response<SimplePetResponse> deletePet(Integer petId) {
        return apis.deletePet(petId).execute();
    }

    @SneakyThrows
    public Response<SimplePetResponse> uploadImageForPet(Integer petId, MultipartBody.Part image) {
        return apis.uploadImageForPet(petId, null, image).execute();
    }

}
