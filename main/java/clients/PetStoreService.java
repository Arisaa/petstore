package clients;

import model.SimplePetResponse;
import model.pet.Pet;
import model.pet.Status;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface PetStoreService {

    @POST("/pet")
    Call<Pet> addPet(@Body Pet pet);

    @PUT("/pet")
    Call<Pet> updatePet(@Body Pet pet);

    @GET("/pet/findByStatus")
    Call<List<Pet>> getPetsWithStatus(@Query("petId") Status status);

    @GET("/pet/{petId}")
    Call<Pet> getPetWithId(@Path("petId") Integer petId);

    @POST("/pet/{petId}")
    Call<SimplePetResponse> updatePet(@Path("petId") Integer petId,
                        @Query("name") String name,
                        @Query("status") Status status);

    @DELETE("/pet/{petId}")
    Call<SimplePetResponse> deletePet(@Path("petId") Integer petId);


    @Multipart
    @POST("/pet/{petId}/uploadImage")
    Call<SimplePetResponse> uploadImageForPet(@Path("petId") Integer petId,
                                              @Query("additionalMetadata") String additionalMetadata,
                                              @Part MultipartBody.Part image);

}
