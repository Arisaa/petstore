package core;

import lombok.NoArgsConstructor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@NoArgsConstructor
public class ApiCaller {

    public <T> T getServiceApiCalls(String url, Class<T> clazz) {
        return createRetrofitClientForUrl(url).create(clazz);
    }

    private Retrofit createRetrofitClientForUrl(String url) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
