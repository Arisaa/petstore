package assertions;

import model.pet.Category;
import model.pet.Pet;
import model.pet.Status;
import model.pet.Tag;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;

import java.util.List;

public class PetAssert extends AbstractAssert<PetAssert, Pet> {

    SoftAssertions softly = new SoftAssertions();

    private PetAssert(Pet actual) {
        super(actual, PetAssert.class);
    }

    public static PetAssert assertThat(Pet actual) {
        return new PetAssert(actual);
    }

    public static PetAssert of(Pet actual) {
        return new PetAssert(actual);
    }

    public PetAssert isNotEmpty() {
        Assertions.assertThat(actual)
                .isNotNull();
        return this;
    }

    public PetAssert isSameAs(Pet pet){
         Assertions.assertThat(actual)
                 .isEqualTo(pet);
         return this;
    }

    public PetAssert hasId(int id) {
        Assertions.assertThat(actual.getId())
                .isEqualTo(id);
        return this;
    }

    public PetAssert hasExpectedCategory(Category category) {
        Assertions.assertThat(actual.getCategory())
                .isEqualTo(category);
        return this;
    }

    public PetAssert hasName(String name) {
        Assertions.assertThat(actual.getName())
                .isEqualTo(name);
        return this;
    }

    public PetAssert hasExpectedPhotoUrl(List<String> photoUrls) {
        Assertions.assertThat(actual.getPhotoUrls())
                .isEqualTo(photoUrls);
        return this;
    }

    public PetAssert hasExpectedTags(List<Tag> tags) {
        Assertions.assertThat(actual.getTags())
                .isEqualTo(tags);
        return this;
    }

    public PetAssert hasStatus(Status status) {
        Assertions.assertThat(actual.getStatus())
                .isEqualTo(status);
        return this;
    }

}
