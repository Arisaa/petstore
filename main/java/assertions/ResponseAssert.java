package assertions;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import retrofit2.Response;

public class ResponseAssert extends AbstractAssert<ResponseAssert, Response> {

    private <T> ResponseAssert(Response<T> actual) {
        super(actual, ResponseAssert.class);
    }

    public static <T> ResponseAssert assertThat(Response<T> actual) {
        return new ResponseAssert(actual);
    }

    public <T extends AbstractAssert> T and(T assertObject) {
        return assertObject;
    }

    public ResponseAssert hasStatus(int statusCode) {
        Assertions.assertThat(actual.code())
                .as("Status code should be %d", statusCode)
                .isEqualTo(statusCode);
        return this;
    }
}
