package assertions;

import model.pet.Pet;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;

import java.util.List;

public class PetsAssert extends AbstractAssert<PetsAssert, List<Pet>> {

    private PetsAssert(List<Pet> actual) {
        super(actual, PetsAssert.class);
    }

    public static PetsAssert assertThat(List<Pet> actual) {
        return new PetsAssert(actual);
    }

    public static PetsAssert of(List<Pet> actual) {
        return new PetsAssert(actual);
    }

    public PetsAssert isNotEmpty() {
        Assertions.assertThat(actual)
                .isNotNull();
        return this;
    }

    public PetsAssert containsPet(Pet pet){
         Assertions.assertThat(actual)
                 .contains(pet);
         return this;
    }
}
