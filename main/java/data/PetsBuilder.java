package data;

import model.pet.Category;
import model.pet.Pet;
import model.pet.Tag;

import static java.util.Collections.singletonList;
import static model.pet.Status.AVAILABLE;

public class PetsBuilder {

    public static Pet getFluffy() {
        return Pet.builder()
                .id(0)
                .category(Category.builder()
                        .id(0)
                        .name("Dog")
                        .build())
                .name("Fluffy")
                .photoUrls(singletonList("url"))
                .tags(singletonList(Tag.builder()
                        .id(0)
                        .name("brown")
                        .build()))
                .status(AVAILABLE)
                .build();
    }

    public static Pet getRubens() {
        return Pet.builder()
                .id(1)
                .category(Category.builder()
                        .id(1)
                        .name("Cat")
                        .build())
                .name("Rubens")
                .photoUrls(singletonList("url"))
                .tags(singletonList(Tag.builder()
                        .id(1)
                        .name("ginger")
                        .build()))
                .status(AVAILABLE)
                .build();
    }
}
