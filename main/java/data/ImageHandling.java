package data;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import java.io.File;

public class ImageHandling {

    public static MultipartBody.Part prepareFilePart() {

        File file = new File("main/resources/Rubka.jpg");

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        return MultipartBody.Part.createFormData("image", file.getName(), requestFile);
    }
}
