package model.pet;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Status {
    AVAILABLE("available"),
    PENDING("pending"),
    SOLD("sold");

    String status;
}
