package model.pet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class Pet {

    private Category category;

    private Integer id;

    private String name;

    private List<String> photoUrls;

    private Status status;

    private List<Tag> tags;

}
