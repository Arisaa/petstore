# README #

### JDK - 1.8 ###

### To run tests: ###

* Import all maven dependencies (pom attached)
  Example instruction on how to do it using IntelIJ:
  https://www.lagomframework.com/documentation/1.6.x/java/IntellijMaven.html
  
* Run test suite "tests.xml"
