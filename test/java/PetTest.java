import assertions.PetAssert;
import assertions.PetsAssert;
import clients.PetStoreClient;
import core.ApiCaller;
import model.SimplePetResponse;
import model.pet.Pet;
import model.pet.Status;
import okhttp3.MultipartBody;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import retrofit2.Response;

import java.util.Arrays;
import java.util.List;

import static assertions.ResponseAssert.assertThat;
import static data.ImageHandling.prepareFilePart;
import static data.PetsBuilder.getFluffy;
import static data.PetsBuilder.getRubens;
import static model.pet.Status.PENDING;
import static model.pet.Status.SOLD;

public class PetTest {

    private final ApiCaller apiCaller = new ApiCaller();
    private final PetStoreClient petStoreClient = new PetStoreClient(apiCaller);

    @DataProvider
    public Object[][] animals() {
        return new Object[][]{
                {getFluffy()},
                {getRubens()}};
    }


    @Test(dataProvider = "animals")
    public void addPetShouldReturnCorrectResponse(Pet pet) {
        // given
        int expectedCode = 200;

        // when
        Response<Pet> response = petStoreClient.addPet(pet);

        // then
        assertThat(response)
                .hasStatus(expectedCode)
                .and(PetAssert.assertThat(response.body()))
                .isNotEmpty()
                .isSameAs(pet);
    }

    @Test(dataProvider = "animals")
    public void updatingPetStatusShouldChangeIt(Pet pet) {
        // given
        int expectedCode = 200;

        // when
        petStoreClient.addPet(pet);
        pet.setStatus(SOLD);
        // and
        Response<Pet> response = petStoreClient.updatePet(pet);

        // then
        assertThat(response)
                .hasStatus(expectedCode)
                .and(PetAssert.assertThat(response.body()))
                .isNotEmpty()
                .hasStatus(SOLD);
    }

    @Test(dataProvider = "animals")
    public void existingPetShouldBeReturnedByStatus(Pet pet) {
        // given
        int expectedCode = 200;

        // when
        petStoreClient.addPet(pet);
        // and
        Response<List<Pet>> response = petStoreClient.getPetsWithStatus(pet.getStatus());

        // then
        assertThat(response)
                .hasStatus(expectedCode)
                .and(PetsAssert.assertThat(response.body()))
                .isNotEmpty()
                .containsPet(pet);
    }

    @Test(dataProvider = "animals")
    public void existingPetShouldBeReturnedById(Pet pet) {
        // given
        int expectedCode = 200;

        // when
        petStoreClient.addPet(pet);
        // and
        Response<Pet> response = petStoreClient.getPet(pet.getId());

        // then
        assertThat(response)
                .hasStatus(expectedCode)
                .and(PetAssert.assertThat(response.body()))
                .isNotEmpty()
                .isSameAs(pet);
    }

    @Test(dataProvider = "animals")
    public void notExistingPetShouldNotBeReturnedById(Pet pet) {
        // given
        int expectedCode = 404;

        // when
        Response<Pet> response = petStoreClient.getPet(pet.getId());

        // then
        assertThat(response)
                .hasStatus(expectedCode);
    }

    @Test(dataProvider = "animals")
    public void updatingPetDataShouldChangeIt(Pet pet) {
        // given
        int expectedCode = 200;
        String newName = "Teddy";
        Status newStatus = PENDING;

        // when
        petStoreClient.addPet(pet);
        petStoreClient.updatePet(pet.getId(), newName, newStatus);
        // and
        Response<Pet> response = petStoreClient.getPet(pet.getId());

        // then
        assertThat(response)
                .hasStatus(expectedCode)
                .and(PetAssert.assertThat(response.body()))
                .isNotEmpty()
                .hasName(newName)
                .hasStatus(newStatus);
    }

    @Test(dataProvider = "animals")
    public void deletedPetShouldNotBeReturnedById(Pet pet) {
        // given
        int expectedCode = 404;

        // when
        petStoreClient.addPet(pet);
        petStoreClient.deletePet(pet.getId());
        // and
        Response<Pet> response = petStoreClient.getPet(pet.getId());

        // then
        assertThat(response)
                .hasStatus(expectedCode);
    }

    @Test(dataProvider = "animals")
    public void uploadingImageForPetShouldAddIt(Pet pet) {
        // given
        int expectedCode = 200;
        MultipartBody.Part image = prepareFilePart();

        // when
        petStoreClient.addPet(pet);
        // and
        Response<SimplePetResponse> response = petStoreClient.uploadImageForPet(pet.getId(), image);

        // then
        assertThat(response)
                .hasStatus(expectedCode);
    }

    @AfterMethod
    private void cleanUp() {
        Arrays.stream(Status.values())
                .forEach(status -> {
                    List<Pet> pets = petStoreClient.getPetsWithStatus(status).body();
                    if (pets != null) {
                        pets.forEach(pet -> petStoreClient
                                .deletePet(pet.getId()));
                    }
                });
    }
}
